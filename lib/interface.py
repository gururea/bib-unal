# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

#Dependencies (package and system) are checked here.
#All messages should be printed from here.

import formula as BBFormula
import utils
import shutil

from installer import InstallerFactory
from database import *
from packageversion import *
from config import *
from errors import *

import glob
import os
import re
import subprocess
import sys

TREE = [(d, os.path.join(BIB_ACTIVE, d)) for d in BIB_DIRS]

#TODO : this fxn is actually is not used, can we get rid of it?
def resolveConflictPrompt(conflict_list,int_force):
  print "Warning! Conflict between:\n"
  for conflict in conflict_list:
    print "Package: ", conflict[0]
    print "Version: ", conflict[1]
    print "\n"
  if (int_force == 0):
    print "Both the new package and the conflicting package(s) may not be active at the same time.  Select 1 to deactivate ALL installed, conflicting packages and continue with activation of your selected package (this is the default).\n"
    print "Or, select 2 to keep the conflicting packages activated and keep the selected package in a deactivated state."
    print "Note: you can always activate and deactivate packages at any time."

    choice = raw_input('Enter 1 or 2:')
    if choice == '2':
      return 2
    else:
      return 1
  else:
    return 1

def check_error_obj(cmpd_err_obj):
  if not(cmpd_err_obj.isEmpty()):
    raise cmpd_err_obj

#headerFileCheck()
#checks if str_headername is a valid C or CPP header file
def headerFileCheck(compiler, str_headername):
  test_file=open("test.c",'w')
  if (str_headername != None):
    test_file.write("#include \"" + str_headername +".h\"\n")
  test_file.write("int main() {return 0;}\n")
  test_file.close()
  if (compiler == 'g++'):
    COMP = BIB_DEVKIT + "/bin/g++"
  else:
    COMP = BIB_DEVKIT + "/bin/gcc"
  try:
    rc = subprocess.check_call([COMP,"test.c"])
    os.remove("test.c")
    os.remove("a.out")
  except:
    os.remove("test.c")
    raise
  return rc

def systemDependCheckerErrorMessage(str_depend):
  return "The dependency " + str_depend + " is not installed.  You need to install it through your OS or package manager before installing this recipe."

def systemDependChecker(pv):
  utils.info("Checking system dependency " + pv.name)

  success = 1
  msg = ""
  if pv.name == 'g++':
    try:
      headerFileCheck('g++',None)
    except:
      success = 0
      msg = "Couldn't compile a simple g++ program.  You need to install g++ 4.4 through your OS or package manager before installing this recipe."
  elif pv.name == 'gcc':
    try:
      rc = headerFileCheck('gcc',None)
    except:
      success = 0
      msg = "Couldn't compile a simple gcc program.  You need to install gcc 4.4 through your OS or package manager before installing this recipe."
  elif pv.name == 'curses':
    try:
      rc = headerFileCheck('gcc','curses')
    except:
      success = 0
      msg = systemDependCheckerErrorMessage('curses')

  elif pv.name == 'java':
    try:
      rc = subprocess.check_call(["java","-version"])
    except:
      success = 0
      msg = systemDependCheckerErrorMessage(pv.name)
  elif pv.name == 'cmake':
    try:
      rc = subprocess.check_call(["cmake","--version"])
    except:
      success = 0
      msg = systemDependCheckerErrorMessage(pv.name)
  elif pv.name == 'zlib':
    try:
      rc = headerFileCheck('gcc','zlib')
    except:
      success = 0
      msg = systemDependCheckerErrorMessage('zlib')
#TODO insert more system depends logic here
  return (success,msg)

class BBInterface(Database):

  def __init__(self,dict1 = {'verbose': 0, 'force': 0}):
    self.dict_global_opts = dict1
    super(BBInterface,self).__init__()

  def vprint(self,obj):
    if self.dict_global_opts['verbose'] == 1:
      print obj

  """
  Deactivate a package

  Inputs: A PackageVersion

  Outputs: void
  """
  def deactivatePackage(self, version): #TODO: contingent on how we decide to structure the database
    error_obj = CompoundError(None)
    package_status = super(BBInterface,self).getPackageStatus(version)
    if not(package_status.isInstalled) or not(package_status.isActive):
      error_obj.addMsg("This package is not installed, or is not active")
    reverse_depends_list = super(BBInterface,self).getReverseDepends(version)     #check for reverse dependencies
    if len(reverse_depends_list) > 0:
      for pkg in reverse_depends_list:
        error_obj.addMsg("Deactivation error: Installed and active package "+ str(pkg) + " depends on this package.  Please deactivate/uninstall it first.")
    check_error_obj(error_obj)
    utils.info("OK to deactivate")  #should be okay to deactivate from here
    super(BBInterface,self).removeInPlaceDep(version)
    formula = BBFormula.getBBFormulaFromPath(version,super(BBInterface,self).getPath(version))
    self.vprint(formula)
    utils.info("Formula file read successfully (for deactivation)")
    for subdir, dstdir in TREE:
      for pattern in formula[subdir]:
        for f in glob.glob(os.path.join(formula.prefix, pattern)):
          dst = os.path.join(dstdir, os.path.basename(f))
          try:
            if (os.path.islink(dst)):
              os.remove(dst)
              utils.info("removed " + dst)
          except Exception as e:
            error_obj.addMsg("unable to remove:" + dst)
            #utils.info("unable to remove:", dst)
    check_error_obj(error_obj)
    super(BBInterface,self).deactivateVersion(version)
    utils.info("Deactivation completed")

  """
  Note: this function only attempts to activate the package, it could still be deactivated at the end if there are conflicting packages the user wants to keep active for example.

 Inputs: A BBFormulaObj

 Ouputs: void

  """
  def activatePackage(self,version):
    error_obj = CompoundError()
    package_status = super(BBInterface,self).getPackageStatus(version)
    if (package_status.isActive):
      print "This package appears to already be active.  Not activating."
      return
    #check for conflicts
    #actual_conflicts_list = super(BBInterface,self).getConflicts(version) #TODO: this should get only active and conflicting packages
    formula = BBFormula.getBBFormulaFromPath(version,super(BBInterface,self).getPath(version))
    self.vprint(formula)
    utils.info("Formula file read successfully (for activation)")
    for dependency in formula['depends']: #check dependencies
      satisfied = 0
      satisfying_list = self.getSatisfyingPackages(dependency) #get a list of possible packages to satisfy the dependency
      for pkg in satisfying_list:
        if (pkg.isInstalled and pkg.isActive):
          satisfied = 1 #dependency satisfied, move on
          break
      if (satisfied == 0):
        error_obj.addMsg("Dependency " + str(dependency) + " is not installed or is inactive.")
    conflict_obj_list = formula['conflicts']
    conflict_obj_list.append(PackageDependency(version.name, 'any','*'))
    for conflict_obj in conflict_obj_list: #check conflicts
      conflict_list = self.getSatisfyingPackages(conflict_obj)
      for pkg in conflict_list: #get a list of all packages that would conflict with this package
        if (pkg.isInstalled and pkg.isActive):
          error_obj.addMsg("Activation error: Active package " + str(pkg) + " conflicts with this package.")
    check_error_obj(error_obj)
    for subdir, dstdir in TREE:
      for pattern in formula[subdir]:
        for f in glob.glob(os.path.join(formula.prefix, pattern)):
          dst = os.path.join(dstdir, os.path.basename(f))
          if not os.path.exists(f):
            error_obj.addMsg("unable to find: %s" % f)
          elif os.path.exists(dst):
            utils.info("file already exists: %s" % dst)
          else:
            try:
              os.symlink(f, dst)
            except:
              error_obj.addMsg("unable to link: %s -> %s" % (f, dst))
    check_error_obj(error_obj)
    utils.info("Running test commands")
    for cmd in formula['test_inst_cmds']:
      ret_code = subprocess.call(cmd.split())
      if ret_code != 0:
        raise InstallationErrorMsg("Test command \"" + cmd + "\" failed")
    super(BBInterface,self).activateVersion(version)
    utils.info("Activation completed")


  """
  Installs a package.

  Inputs: A version
  """
  def installPackage(self,version):
    error_obj = CompoundError()
    package_status = super(BBInterface,self).getPackageStatus(version)
    if (package_status.isInstalled):
      raise InstallationErrorMsg("Package " + version.__str__() + " appears to be already installed.")
    formula = BBFormula.getBBFormulaFromPath(version,super(BBInterface,self).getPath(version))
    self.vprint(formula)
    utils.info("Formula file read successfully (for installation)")

    for system_depend in formula['system-depends']:
      (depend_success,msg1) = systemDependChecker(system_depend)
      if depend_success == 0:
        error_obj.addMsg(msg1)

    installed_deps_list = []
    for dependency in formula['depends']: #check dependencies
      satisfied = 0
      satisfying_list = self.getSatisfyingPackages(dependency) #get a list of possible packages to satisfy the dependency
      for pkg in satisfying_list:
        if (pkg.isInstalled and pkg.isActive):
          satisfied = 1 #dependency satisfied, move on
          installed_deps_list.append(pkg)
          break
      if (satisfied == 0):
        error_obj.addMsg("Dependency " + str(dependency) + " is not installed or is inactive.")
    #TODO - conflicts don't have to be checked at installation, only activation.  It should be possible for any two packages to be installed at the same time.
    #for conflict_obj in formula['conflicts']: #check conflicts
    #  conflict_list = self.getSatisfyingPackages(conflict_obj)
    #  for pkg in conflict_list: #get a list of all packages that would conflict with this package
    #    if (pkg.isInstalled and pkg.isActive):
    #      error_msgs.append("Installed package " + str(pkg) + " conflicts with this package.")
    check_error_obj(error_obj)
    utils.info("All dependencies checked and no conflicts") #begin install
    installer_obj = InstallerFactory(formula)
    install_sh = installer_obj.writeInstallFile() #write install.sh
    os.chmod(install_sh, 0755)
    if (self.dict_global_opts['force'] == 0):
      print "Ready to install.  Press ENTER to continue with the install."
      junk = raw_input()
    return_code = subprocess.call(install_sh) #execute install.sh
    if return_code==0:
      utils.info("Install file executed successfully")
      utils.info("Marking as installed in the DB")
      super(BBInterface,self).installPackage(version)
      for dependency_pkg in installed_deps_list:
        super(BBInterface,self).addInPlaceDep(version,dependency_pkg)
    else:
      error_obj.addMsg("There was an error executing the install script, please contact the support team.  Include a copy of your install file (in /tmp) and the output of the install command showing where the error happened.")
      if os.path.isdir(formula.prefix):
        shutil.rmtree(formula.prefix)
      raise CompoundError(error_obj)
    try:
      self.activatePackage(version)
    except CompoundError as e:
      utils.info("Installation was successful, but activation failed, with the following messages:")
      for msg in e.msg_list:
        print msg
    os.remove(install_sh) #install file gets left behind only if error
    utils.info("Installation completed")

  """
  Uninstalls a package

  Inputs: a version
  """

  def uninstallPackage(self,version):
    package_status = super(BBInterface,self).getPackageStatus(version)
    if not(package_status.isInstalled):
      raise PackageNotInstalledError(version)
    formula = BBFormula.getBBFormulaFromPath(version,super(BBInterface,self).getPath(version))
    self.vprint(formula)
    utils.info("Formula file read successfully (for uninstallation)")
    utils.info("Deactivating package first")
    if (package_status.isActive):
      self.deactivatePackage(version)
    if (self.dict_global_opts['force'] == 0):
      print "Ready to uninstall.  Press ENTER to continue with the uninstall."
      junk = raw_input()
    install_obj = InstallerFactory(formula)
    uninstall_sh = install_obj.writeUninstallFile()

    os.chmod(uninstall_sh, 0755)
    return_code = subprocess.call(uninstall_sh) #execute install.sh
    if return_code==0:
      utils.info("Uninstall file executed successfully")
      utils.info("Marking as uninstalled in the DB")
      super(BBInterface,self).uninstallPackage(version)
    else:
      raise InstallationErrorMsg("There was an error executing the uninstall script, please contact the support team.  Include a copy of your install file (in /tmp) and the output of the install command showing where the error happened.")
    os.remove(uninstall_sh)
    #check if any versions of this package are installed any more.  If not, we can safely remove the directory.
    any_installed = False
    ps_list = self.getSatisfyingPackages(PackageRange(version.name,'','*'))
    for ps in ps_list:
      if ps.isInstalled:
        any_installed = True
        break
    if any_installed == False:
      os.rmdir(os.path.join(BIB_INSTALL, version.name))
    utils.info("Uninstallation completed")

  def DBInstallPackage(self,version):
    super(BBInterface,self).installPackage(version)

  def DBActivatePackage(self,version):
    super(BBInterface,self).activateVersion(version)

# vim: autoindent expandtab ts=2 shiftwidth=2
