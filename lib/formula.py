# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

import ConfigParser

from errors import *
from config import *
from packageversion import *


DEFAULTS = {
  'BIB_PREFIX': BIB_PREFIX,
  'BIB_INSTALL': BIB_INSTALL,
  'BIB_ACTIVE': BIB_ACTIVE,
  'BIB_ARCH': BIB_ARCH}


class Formula(dict):
  """
  A mutable alternative to namedtuple that supports accessing values as
  attributes or with the dict [] operator.
  """

  def __init__(self, *args, **kwargs):
    super(Formula, self).__init__(*args, **kwargs)

    self['name'] = None
    self['version'] = None
    self['homepage'] = None
    self['url'] = None
    self['checksum'] = None
    self['checksum_method'] = None

    # A list of PackageVersion objects, describing dependent softwares
    self['system-depends']= []
    self['depends'] = []

    # A list of PackageVersion objects, describing conflicts
    self['conflicts'] = []

    """
    *********************
    TOOLCHAIN:
    *********************
    specifying a specific toolchain like GNU or Cmake allows you to install without providing any of the special custom commands like prefix_cmds, build_cmds, and postfix_cmds.
    Here are the specific toolchains supported and what they do.

    gnu:
    ----
    -cd to the unpacked source directory.
    -Run ./configure with the specified build options.
    -Run make
    -Run make install

    Custom:
    ----
    -run the list of commands specified in the "build_cmds" entry

    cmake:
    ----

    default: gnu
    """
    self['toolchain'] = 'gnu' #one of gnu, custom, or cmake
    self['build_options'] = ''

    """
    This is the name of the directory that is created when the file is unzipped.
    Where the file is unzipped depends on the toolchain.

    Toolchain: File unzipped to
    gnu, cmake: BASE/src
    custom: BASE
    """
    self['source_dir'] = None

    # Paths that are linked from the install directory into the links
    # directory.
    for d in BIB_DIRS:
      self[d] = []

    # Shell commands to be run before the install script.  Commands start in
    # the prefix directory by default.
    self['prefix_cmds'] = []

    # Any custom build commands go here. These start in the unpacked source
    # directory ("source_dir", above) by default.
    self['build_cmds'] = []

    # Shell commands to be run after the install script. Commands start in the
    # prefix directory by default.
    self['postfix_cmds'] = []

    # Shell commands to be run as a test, to see if the install was successful.
    # These start in the BIB_PREFIX by default.  All commands must run
    # successfully (return code = 0) in order for the install to be considered
    # successful.
    self['test_inst_cmds'] = []

    self['prefix'] = None

    self._initialized = True

  def __getattr__(self, name):
    try:
      return self[name]
    except KeyError:
      raise AttributeError(name)

  def __setattr__(self, name, value):
    if hasattr(self, '_initialized'):
      super(Formula, self).__setitem__(name, value)
    else:
      super(Formula, self).__setattr__(name, value)

  def __str__(self):
    str_rep = ["FORMULA:"]
    str_rep += [" %s: %s" % (key, self[key]) for key in sorted(self)]
    return '\n'.join(str_rep)


"""
inputs:
returns a list of PackageComparableVersions in the formula file
"""
def parseFormulaFile(fname):

  filename = os.path.join(BIB_RECIPES, fname) #TODO: should probably replace this with a pull from the databasea

  rcp = ConfigParser.SafeConfigParser(DEFAULTS)

  #validate file
  try:
    rcp.read(filename)
  except:
    raise FormulaFileException

  version_sections = rcp.sections()
  pv_list = []

  name = rcp.get("default", "name")

  for version_section in version_sections:
    if version_section != "default":
      release_date = version_section.split('_v_')[0]
      version = version_section.split('_v_')[1]
      recipe_arch = version_section.split('_v_')[2]
      if recipe_arch == 'ALL' or BIB_ARCH.startswith(recipe_arch):
        pv = PackageComparableStatus(name, version, release_date)
        pv_list.append(pv)
  return pv_list

"""
BBFormulaForVersion:
inputs:
name - the name of a package
version - version number to get within the formula

returns:
BBFormula object specifying instructions for that software+version combination.
"""
def getBBFormulaFromPath(pv, filename):

  formula = Formula()
  formula.name = pv.name
  formula.version = pv.version
  formula.prefix = os.path.join(BIB_INSTALL, formula.name, formula.version)

  defaults = DEFAULTS.copy()
  defaults['prefix'] = formula.prefix

  rcp = ConfigParser.SafeConfigParser(defaults)

  #validate file
  try:
    rcp.read(filename)
  except:
    raise FormulaFileException

  try:

    version_sections = rcp.sections()
    match_found = 0
    for version_section in version_sections:
      if version_section != "default":
        package_version= version_section.split('_v_')[1]
        recipe_arch = version_section.split('_v_')[2]
        if (package_version == pv.version) and (recipe_arch == 'ALL' or BIB_ARCH.startswith(recipe_arch)):
          match_found = 1
          version_str = version_section
    if match_found == 0:
      raise MissingFormulaFileEntryException(pv)

    #if not(rcp.has_section(pv.version)):
    #  raise NoVersionPresentException

    #this dictionary directs how to parse the different entries in the INI file.  All entries with the same dictionary key will be parsed the same way.
    possible_opts = {
      'single': (
        'homepage', 'url',
        'toolchain', 'build_options',
        'filename', 'source_dir'),
      'list': BIB_DIRS,
      'commands': ('prefix_cmds', 'postfix_cmds', 'build_cmds', 'test_inst_cmds'),
      'checksum': ('sha256', 'sha1'),
      'dependencies': ('depends', 'system-depends', 'conflicts')
    }

    for opt_type in possible_opts:
      for opt in possible_opts[opt_type]:
        # this option wasn't specified, leave it as the default
        if rcp.has_option(version_str, opt):
          # real_version one of two strings ('default' or version number) that
          # specifies whether to use the default setting or the
          # version-specific setting, at the time of option lookup.
          real_version = version_str
        elif rcp.has_option('default', opt):
          real_version = 'default'
        else: #the option wasn't included at all, leave uninitialized
          continue

        if opt_type == 'single':
          formula[opt] = rcp.get(real_version, opt)
        elif opt_type == 'list':
          opt_str = rcp.get(real_version, opt)
          opt_arr = opt_str.split(' ')
          if opt_arr[0] != '': formula[opt] = opt_arr
        elif opt_type == 'commands':
          opt_str = rcp.get(real_version, opt)
          opt_arr = opt_str.split(';')
          if opt_arr[0] != '': formula[opt] = opt_arr
        elif opt_type == 'checksum':
          formula['checksum'] = rcp.get(real_version, opt)
          formula['checksum_method'] = opt
        elif opt_type=='dependencies':
          opt_str=rcp.get(real_version, opt)
          opt_arr=opt_str.split(',')
          if opt_arr[0]=='':
            pass
          else:
            for pkg_version in opt_arr:
              pkg = pkg_version.split('/')[0] #the package name
              dep_sign = pkg[0]
              if (dep_sign!='+' and dep_sign!='-' and dep_sign!='=' and dep_sign!='*'):
                raise Exception
              pkg_name = pkg[1:]
              version = pkg_version.split('/')[1] #manufacturer's version number
              if version=='any':
                formula[opt].append(PackageDependency(pkg_name,'0','*'))
              else:
                formula[opt].append(PackageDependency(pkg_name,version,dep_sign))

  except:
    raise

  return formula

# vim: autoindent expandtab ts=2 shiftwidth=2
