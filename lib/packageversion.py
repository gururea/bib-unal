# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

"""
######################
VERSIONS
######################

Versions in formula files should have the syntax:

<YYYYMMDD>_v_<official version number from the developer>

For example, the software version of Samtools 0.1.19, released on March 19, 2013, would have the version string:

20130319_v_0.1.19



######################
DEPENDENCIES
######################

We won't automatically resolve dependencies for now.  You have to install all dependencies manually.

Dependencies use a very specific syntax:
<sign><name>/<version number, or "any">

For example:
+samtools/any - requires samtools, version doesn't matter
+samtools/0.1.19 - requires samtools version 0.1.19 or greater.
=samtools/0.1.19 - requires exactly samtools version 0.1.19.
-samtools/0.1.19 - requires samtools version 0.1.19 or less.

"""

"""
PackageVersion
"""
import datetime
from errors import *

class PackageVersion(object):
  def __init__(self,p='',v=''):
    self.name=p
    self.version=v #the developer's version number

  def __str__(self):
    return self.name + "/" + self.version


class PackageNoVersion(PackageVersion):
  def __init__(self, package_name, bb_interface):
    self.name = package_name
    new_pr = PackageRange(package_name, '0', '*')
    ps_list = bb_interface.getSatisfyingPackages(new_pr)
    if len(ps_list) == 0:
      raise PackageNotFoundError(package_name)
    installed_pkg_count = 0
    tgt_version = ''
    for ps in ps_list: #count the installed versions
      if ps.isInstalled:
        installed_pkg_count += 1
        tgt_version = ps.version
    if installed_pkg_count == 0:
      raise PackageNotInstalledError(new_pr)
    elif installed_pkg_count > 1:
      raise NoVersionSpecifiedException
    else:
      self.version = tgt_version


"""
PackageStatus
"""
class PackageStatus(PackageVersion):
  def __init__(self,p='',v='',installed=False,active=False):
    super(PackageStatus,self).__init__(p,v)
    self.isInstalled=installed
    self.isActive=active

  def __str__(self):
    return super(PackageStatus,self).__str__()


"""
PackageComparableVersion
"""
class PackageComparableStatus(PackageStatus):
  def __init__(self,p='',v='',r=0,installed=False,active=False):
    super(PackageComparableStatus,self).__init__(p,v,installed,active)
    self.releasedate=r

  def __lt__(self,other):
    return self.releasedate < other.releasedate

  def __le__(self,other):
    return (self < other or self==other)

  def __eq__(self,other):
    return (self.version==other.version and self.name==other.name)

  def __ne__(self,other):
    return (self.version!=other.version or self.name!=other.name)

  def __gt__(self,other):
    return self.releasedate > other.releasedate

  def __ge__(self,other):
    return (self > other or self==other)

  def __str__(self):
    return super(PackageComparableStatus,self).__str__()


"""
PackageRange

Class to describe a "range" of software versions.

Functions
---
checkSatisfies()
input: PackageComparableStatus object (specific version of software)
output: if input PCS falls within the "range", then true.  Else false.

"""
class PackageRange(PackageVersion):
  def __init__(self,p='',v='',sign='='):
    super(PackageRange,self).__init__(p,v)
    self.sign=sign

  #check if a PackageVersion satisfies this range
  def checkSatisfies(self,pcs):
    if not(pcs.isInstalled) or not(pcs.isActive):
      return False
    if self.version=='*': #any version will do
      return pcs.name == self.name
    elif self.sign=='=':
      return self == pcs
    elif self.sign=='+':
      return pcs >= self
    else:
      return pcs <= self

  def __str__(self):
    if self.sign=='*':
      return self.name
    else:
      return self.name + '/' + self.version

class PackageDependency(PackageRange):
  def __init__(self,p='',v='',sign='='):
    super(PackageDependency,self).__init__(p,v,sign)

class PackageConflict(PackageRange):
  pass
