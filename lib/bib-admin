#!/usr/bin/env python
# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

import sys
import os
import re
import interface as bbi

from formula import *
from packageversion import *
from config import *
from errors import *
import utils

def usageMsg():
  print "Options:\n"
  print "bib-admin parse <recipe-name> : reparse recipe, adding any versions to the database that were not present before"
  print "bib-admin rebuild : reparse all recipes, adding any versions to the database that were not present before."
  print "bib-admin remove <recipe-name> : remove a formula from the master database, removing all versions from the database"
  print "bib-admin create <name> <url> : download a package to cache and create a barebones formula for it"
  sys.exit()

class SavedDependency(object):
  def __init__(self, pv1, pv2):
    self.packageversion1 = pv1
    self.packageversion2 = pv2

if len(sys.argv) < 2:
  usageMsg()

try:
  bb_interface = bbi.BBInterface()

  if (sys.argv[1] == 'rebuild'):
    #store package state
    saved_ps_list = bb_interface.listAll()
    #store dependency information
    dependency_pair_list = bb_interface.getInPlaceDeps()

    os.remove(BIB_DB)
    bb_interface = bbi.BBInterface() #invoke constructor again

    #for ps in saved_ps_list: #remove all existing formulas from the database
    #  pv = PackageVersion(ps.packagename,ps.version)
    #  bb_interface.removeFormula(pv)
    formulas = os.listdir(BIB_RECIPES) #now, rebuild from scratch
    for formulafile in formulas:
      try:
        pv_list = parseFormulaFile(formulafile)
      except Exception as e:
        print "ERROR: Ignoring file", formulafile
        print e
        continue
      for pv in pv_list:
        bb_interface.newFormula(pv, os.path.join(BIB_RECIPES, formulafile))
    for ps in saved_ps_list:
      pv = PackageVersion(ps.name,ps.version)
      if ps.isInstalled==True:
        bb_interface.DBInstallPackage(pv)
      if ps.isActive==True:
        bb_interface.DBActivatePackage(pv)
    for pair in dependency_pair_list:
      bb_interface.addInPlaceDep(pair[0],pair[1])


  elif (sys.argv[1] == 'parse'):
    if len(sys.argv) == 3:
      formula_name=sys.argv[2]
      pv_list = parseFormulaFile(os.path.join(BIB_RECIPES, formula_name + ".bb"))
      for pv in pv_list:
        print(bb_interface.getPackageStatus(pv))
        bb_interface.newFormula(pv,BIB_RECIPES + "/" + formula_name + ".bb")
    else:
      usageMsg()

  elif (sys.argv[1] == 'remove-formula'):
    if len(sys.argv) == 3:
      formula_name=sys.argv[2]
      pv_list = parseFormulaFile(formula_name)
      for pv in pv_list:
        if (bb_interface.getPackageStatus(pv).isInstalled == True):
          bb_interface.removeFormula(pv)
    else:
      usageMsg()

  elif (sys.argv[1] == 'create'):
    if len(sys.argv) == 4:
      name = sys.argv[2]
      url = sys.argv[3]
      recipe = os.path.join(BIB_RECIPES, name + '.bb')
      if os.path.exists(recipe):
        utils.die("a recipe already exists for", name)
      filename = os.path.basename(url)
      cachedfile = os.path.join(BIB_CACHE, name, filename)
      utils.safe_mkdir(os.path.join(BIB_CACHE, name))
      utils.fetch(url, cachedfile)
      checksum = utils.checksum(cachedfile, method="sha256")
      try:
        version = re.search(r'(\d+\.)+\d+\w*', filename).group(0)
      except AttributeError:
        version = os.path.splitext(filename)[0]
      with open(recipe, 'w') as f:
        print >>f, "[default]"
        print >>f, "name =", name
        print >>f, "homepage ="
        print >>f, "toolchain ="
        print >>f, ""
        print >>f, "[%s_v_%s_v_ALL]" % (
                                  datetime.datetime.now().strftime("%Y%m%d"),
                                  version)
        print >>f, "url =", url
        print >>f, "sha256 =", checksum
      utils.info("recipe written to", recipe)
    else:
      usageMsg()

  else:
    usageMsg()
except Exception:
  raise

# vim: autoindent expandtab ts=2 shiftwidth=2
