# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

import sqlite3 as sql
from errors import *
from config import *
import os
from packageversion import PackageStatus, PackageVersion, PackageComparableStatus

class Database(object):
    '''
Table schemas:

formulas
--------------------------------------------
id package version path isInstalled isActive

conflicts
---------
id1 id2
    '''
    def __init__(self, path=BIB_DB):
        self.conn = sql.connect(path)
        self.cursor = self.conn.cursor()
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS formulas(
                               id INTEGER PRIMARY KEY AUTOINCREMENT,
                               package TEXT NOT NULL,
                               version TEXT NOT NULL,
                               releasedate REAL NOT NULL,
                               path TEXT NOT NULL,
                               isInstalled INTEGER DEFAULT 0,
                               isActive INTEGER DEFAULT 0,
                               unique (package, version) on conflict ignore)''')
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS inplace_deps(
                               id INTEGER,
                               dep_id INTEGER,
                               FOREIGN KEY(id) REFERENCES formulas(id),
                               FOREIGN KEY(dep_id) REFERENCES formulas(id))''')
        self.conn.commit()

    def newFormula(self, pv, formulaPath):
        '''Add a new formula file to the DB, with respect to a specific package & version'''
        self.cursor.execute('''INSERT INTO formulas(package, version, releasedate, path)
                               VALUES(?, ?, ?, ?)''', (pv.name, pv.version, pv.releasedate, formulaPath))
        self.conn.commit()

    def removeFormula(self, pv):
        '''Remove a formula from the DB, with respect to a specific package & version.
           Also remove all related conflicts
        '''
        self.cursor.execute('''DELETE FROM formulas
                               WHERE package = ? AND version = ?''', (pv.name, pv.version))
        self.conn.commit()

    def installPackage(self, pv):
        '''Mark this version of the package as installed'''
        self.cursor.execute('''UPDATE formulas SET isInstalled = 1
                               WHERE package = ? AND version = ?''', (pv.name, pv.version))
        self.conn.commit()

    def getInPlaceDeps(self):
        '''return a list of tuples of packageversions (pv_pkg,pv_dep)'''
        self.cursor.execute('''SELECT * FROM inplace_deps''')
        rows = self.cursor.fetchall()
        # convert rows to PackageStatus objects
        results = []
        for row in rows:
          self.cursor.execute("SELECT package,version from formulas WHERE id=" + str(row[0]))
          pv_pkg_row = self.cursor.fetchone()
          self.cursor.execute("SELECT package,version from formulas WHERE id=" + str(row[1]))
          pv_dep_row = self.cursor.fetchone()
          pv_pkg = PackageVersion(pv_pkg_row[0],pv_pkg_row[1])
          pv_dep = PackageVersion(pv_dep_row[0],pv_dep_row[1])
          results.append((pv_pkg,pv_dep))
        return results

    def addInPlaceDep(self,pv_pkg,pv_dep):
        '''Mark pv_pkg as using pv_dep.  Now pv_dep cannot be removed without removing pv_pkg first'''
        self.cursor.execute('''INSERT INTO inplace_deps values(
                                 (SELECT id FROM formulas WHERE package= ? and version = ?),
                                 (SELECT id FROM formulas WHERE package= ? and version = ?))''',(pv_pkg.name, pv_pkg.version,pv_dep.name,pv_dep.version))
        self.conn.commit()

    def removeInPlaceDep(self,pv_pkg):
        '''For every dep pv_pkg uses, mark pv_pkg as no longer using that pv_dep.  Now pv_dep can be removed safely.'''
        self.cursor.execute('''DELETE FROM inplace_deps where id = (SELECT id FROM formulas WHERE package= ? and version = ?)''',(pv_pkg.name, pv_pkg.version))

    def getReverseDepends(self,pv_pkg):
      self.cursor.execute('''SELECT * from formulas where id = (SELECT id FROM inplace_deps WHERE dep_id = (SELECT id FROM formulas WHERE package= ? and version = ?))''',(pv_pkg.name, pv_pkg.version))
      results = []
      rows = self.cursor.fetchall()
      for row in rows:
        ps = PackageComparableStatus(row[1],row[2],row[3],row[5]==1,row[6]==1)
        results.append(ps)
      return results

    def uninstallPackage(self, pv):
        '''Mark this version of the package as not installed'''
        self.cursor.execute('''UPDATE formulas SET isInstalled = 0
                               WHERE package = ? AND version = ?''', (pv.name, pv.version))
        self.conn.commit()

    def activateVersion(self, pv):
        '''Mark this version of the package as active'''
        self.cursor.execute('''UPDATE formulas SET isActive = 1
                               WHERE package = ? AND version = ?''', (pv.name, pv.version))
        self.conn.commit()

    def deactivateVersion(self, pv):
        '''Mark this version of the package as inactive'''
        self.cursor.execute('''UPDATE formulas SET isActive = NULL
                               WHERE package = ? AND version = ?''', (pv.name, pv.version))
        self.conn.commit()

    def listInstalled(self):
        self.cursor.execute('''SELECT * FROM formulas WHERE isInstalled = 1''')
        rows = self.cursor.fetchall()
        # convert rows to PackageStatus objects
        results = []
        for row in rows:
          ps = PackageComparableStatus(row[1],row[2],row[3],row[5]==1,row[6]==1)
          results.append(ps)
        return results

    def listAll(self):
        self.cursor.execute('''SELECT * FROM formulas''')
        rows = self.cursor.fetchall()
        # convert rows to PackageStatus objects
        results = []
        for row in rows:
          ps = PackageComparableStatus(row[1],row[2],row[3],row[5]==1,row[6]==1)
          results.append(ps)
        return results

    def getPackageStatus(self, pv):
        self.cursor.execute('''SELECT * FROM formulas
                               WHERE package = ? AND version = ?''', (pv.name, pv.version))
        row = self.cursor.fetchone()
        ps = PackageComparableStatus()
        ps.name = pv.name
        ps.version = pv.version
        try:
          ps.releasedate = row[3]
          ps.isInstalled = row[5] == 1
          ps.isActive = row[6] == 1
        except: #if package is not installed
          pass
        return ps

    #get path of the formula file, return as a string
    def getPath(self, pv):
        self.cursor.execute('''SELECT * FROM formulas
                               WHERE package = ? AND version = ?''', (pv.name, pv.version))
        try:
          row = self.cursor.fetchone()
          return row[4]
        except:
          raise MissingFormulaFileEntryException(pv)

    #pd: a packagerange object
    def getSatisfyingPackages(self,pd):
      results = []
      if pd.sign=='*':
        self.cursor.execute('''SELECT * FROM formulas WHERE package = ? ''', (pd.name,))
      elif pd.sign=='=':
        self.cursor.execute('''SELECT * FROM formulas
                               WHERE package = ? AND version = ?''', (pd.name,pd.version))
      elif pd.sign=='-':
        self.cursor.execute('''SELECT * FROM formulas
                               WHERE package = ?
                               AND releasedate <=
                                 (SELECT releasedate FROM formulas
                                  WHERE package = ? AND version = ? )''', (pd.name, pd.name, pd.version))
      else:
        self.cursor.execute('''SELECT * FROM formulas
                               WHERE package = ?
                               AND releasedate >=
                                 (SELECT releasedate FROM formulas
                                  WHERE package = ? AND version = ? )''', (pd.name, pd.name, pd.version))
      rows = self.cursor.fetchall()
      results = []
      for row in rows:
        ps = PackageComparableStatus(row[1],row[2],row[3],row[5]==1,row[6]==1)
        results.append(ps)
      return results

    '''
    def getConflicts(self, pv):
        self.cursor.execute(SELECT * FROM formulas AS F JOIN conflicts as C
                               WHERE package = ? AND version = ? AND (F.id = C.id1 OR F.id = C.id2),
                            (pv.name, pv.version))
        rows = self.cursor.fetchall()
        # convert rows to PackageStatus objects
        results = []
        for row in rows:
          ps = PackageComparableStatus(row[1],row[2],row[3],row[5]==1,row[6]==1)
          results.append(ps)
        return results
    '''

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
