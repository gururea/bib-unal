#!/usr/bin/env python
# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

import subprocess
import sys
from operator import attrgetter

import utils
import interface as bbi
from packageversion import *
from errors import *
from config import BIB_PREFIX

"""
from ginstrom.com
"""
def get_max_width(table, index):
  """Get the maximum width of the given column index"""
  return max([len(row[index]) for row in table])

def pprint_table(out, table):
  """Prints out a table of data, padded for alignment
  @param out: Output stream (file-like object)
  @param table: The table to print. A list of lists.
  Each row must have the same number of columns. """

  col_paddings = []

  for i in range(len(table[0])):
    col_paddings.append(get_max_width(table, i))

  for row in table:
    # left col
    print >> out, row[0].ljust(col_paddings[0] + 1),
    # rest of the cols
    for i in range(1, len(row)):
      col = (row[i]).rjust(col_paddings[i] + 2)
      print >> out, col,
    print >> out

"""
end pasted code
"""

def printPackageList(pkg_list):
  table = []
  table.append(["Name","Version","InstallStatus","ActiveStatus"])
  for pkg in sorted(pkg_list, key=attrgetter('name')):
    active_str = "-"
    install_str = "-"
    if pkg.isInstalled:
      install_str = "Installed"
      if pkg.isActive:
        active_str = "Active"
    table.append([pkg.name,pkg.version,install_str,active_str])
  pprint_table(sys.stdout,table)

def usageMsg():
  print "---"
  print "Bioinformatics Brewery (BiB)\n"
  print "Usage: bib <command> <arguments>"
  print "---\n"
  print "Commands:\n"
  print "install <name>[/<version>] [name2[/<version2>]] ... : install package(s)"
  print "uninstall <name>[/<version>] : uninstall package"
  print "activate <name>/<version> : activate installed package/version combo"
  print "deactivate <name>/<version> : deactivate installed package/version combo"
  print "list <name> : list installed packages"
  print "search : list or search available packages"
  print "update : fetch updated recipes\n"
  print "Options:\n"
  print "--force, -f : do everything non-interactively"
  print "--verbose, -v : print out more information"
  sys.exit()

if len(sys.argv) < 2:
  usageMsg()


dict_global_opts = {'force' : 0, 'verbose' : 0}

#scan for optional arguments.  Only "force" is supported now.

n_argv_size = len(sys.argv)
i_arg = 1

while (i_arg < n_argv_size):
  if sys.argv[i_arg] == "--force" or sys.argv[i_arg] == "-f" :
    dict_global_opts['force'] = 1
    sys.argv.pop(i_arg)
    n_argv_size -= 1
  elif sys.argv[i_arg] == "--verbose" or sys.argv[i_arg] == "-v" :
    dict_global_opts['verbose'] = 1
    sys.argv.pop(i_arg)
    n_argv_size -= 1
  else:
    i_arg+=1

bb_interface = bbi.BBInterface(dict_global_opts)

try:

  if (sys.argv[1] == 'install'):
    arg_list = sys.argv[2:]
    errors_obj = CompoundError()
    for arg in arg_list:
      try:
        arg_split = arg.split('/')
        if (len(arg_split) == 2): #install specific version
          pkg_name=arg_split[0]
          pkg_ver=arg_split[1]
          pv = PackageVersion(pkg_name,pkg_ver)
          bb_interface.installPackage(pv)
        elif (len(arg_split) == 1): #install latest version
          pkg_name = arg_split[0]
          new_pr = PackageRange(pkg_name,'0','*')
          ps_list = bb_interface.getSatisfyingPackages(new_pr)
          if len(ps_list) == 0:
            raise PackageNotFoundError(arg)
          most_recent_ps = ps_list[0]
          for curr_ps in ps_list:
            if curr_ps > most_recent_ps:
              most_recent_ps = curr_ps
          bb_interface.installPackage(most_recent_ps)
        else: #error
          raise PackageNotFoundError(arg)
      except PackageInstallationError as e:
        utils.info("Failed installing package " + arg)
        errors_obj.add(e)
        continue
    if not(errors_obj.isEmpty()):
      raise errors_obj

  elif (sys.argv[1] == 'uninstall'):
    if len(sys.argv) == 3:
      arg_split = sys.argv[2].split('/')
      if len(arg_split) == 1:
        bb_interface.uninstallPackage(
          PackageNoVersion(sys.argv[2], bb_interface))
      elif len(arg_split) == 2: #uninstall specific version
        bb_interface.uninstallPackage(PackageVersion(*arg_split))
      else:
        raise PackageNotFoundError(sys.argv[2])
    else:
      usageMsg()

  elif (sys.argv[1] == 'activate'):
    if len(sys.argv) == 3: #install specific version
      arg_split = sys.argv[2].split('/')
      if len(arg_split) == 1:
        bb_interface.activatePackage(
          PackageNoVersion(sys.argv[2], bb_interface))
      elif len(arg_split) == 2:
        bb_interface.activatePackage(PackageVersion(*arg_split))
      else:
        raise PackageNotFoundError(sys.argv[2])
    else:
      usageMsg()

  elif (sys.argv[1] == 'deactivate'):
    if len(sys.argv) == 3: #install specific version
      arg_split = sys.argv[2].split('/')
      if len(arg_split) == 1:
        bb_interface.deactivatePackage(
          PackageNoVersion(sys.argv[2], bb_interface))
      elif len(arg_split) == 2:
        bb_interface.deactivatePackage(PackageVersion(*arg_split))
      else:
        raise PackageNotFoundError(sys.argv[2])
    else:
      usageMsg()

  elif (sys.argv[1] == 'list'):
    print "Installed packages:"
    package_list = bb_interface.listInstalled()
    printPackageList(package_list)

  elif (sys.argv[1] == 'search'):
    print "All available packages:"
    package_list = bb_interface.listAll()
    printPackageList(package_list)

  elif sys.argv[1] == 'update':
    subprocess.check_call(['git', 'pull'], cwd=BIB_PREFIX)

  else:
    usageMsg()

except PackageInstallationError as e:
  print "WARNING: Some commands failed to execute properly.  Error messages:"
  print e
except NoVersionSpecifiedException:
  print "There is more than one version of this software installed.  Specify the version number when running your command."
except FormulaFileException:
  print "ERROR: Formula file not found!"
except NotImplementedError:
  print "This functionality hasn't been implemented yet"
