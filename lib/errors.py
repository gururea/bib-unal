# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

class InterfaceException(Exception):
  pass

#class PackageNotFoundError(Exception):
#  pass

class PackageInstallationError(Exception):
  pass

class InstallationErrorMsg(PackageInstallationError):
  def __init__(self,msg_obj):
    self.msg = msg_obj

  def __str__(self):
    return self.msg

class CompoundError(PackageInstallationError):
  def __init__(self,err_obj=None):
    self.msg_list = []
    if (err_obj == None):
      pass
    elif (isinstance(err_obj,InstallationErrorMsg)):
      self.msg_list.append(err_obj.msg)
    elif (isinstance(err_obj,CompoundError)):
      self.msg_list.extend(err_obj.msg_list)

  def add(self,err_obj):
    if (isinstance(err_obj,InstallationErrorMsg)):
      self.msg_list.append(err_obj.msg)
    elif (isinstance(err_obj,CompoundError)):
      self.msg_list.extend(err_obj.msg_list)

  def addMsg(self,err_msg):
    self.msg_list.append(err_msg)

  def __str__(self):
    out_str=""
    for msg in self.msg_list:
      out_str += msg + "\n"
    return out_str

  def isEmpty(self):
    return len(self.msg_list) == 0

class PackageNotFoundError(InstallationErrorMsg):
  def __init__(self, pkg):
    super(PackageNotFoundError,self).__init__("Package " + pkg.__str__() + " not found")

class MissingFormulaFileEntryException(InstallationErrorMsg):
  def __init__(self,pkg):
    super(MissingFormulaFileEntryException,self).__init__("Software version " + pkg.__str__() + " has no entry in the formula file")
  pass

class PackageNotInstalledError(InstallationErrorMsg):
  def __init__(self,pkg):
    super(PackageNotInstalledError,self).__init__("Software pkg " + pkg.__str__() + " is not installed")

class FormulaFileException(Exception):
  pass

class NoVersionSpecifiedException(Exception):
  pass
