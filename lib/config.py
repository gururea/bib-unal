# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

import os
import sys

# Paths
BIB_PREFIX = os.getenv('BIB_PREFIX', '/opt/bib')
BIB_DB = os.path.join(BIB_PREFIX, 'lib/db.sqlite')
BIB_RECIPES = os.path.join(BIB_PREFIX, 'recipes')
BIB_INSTALL = os.path.join(BIB_PREFIX, 'install')
BIB_ACTIVE = os.path.join(BIB_PREFIX, 'active')
BIB_CACHE = os.path.join(BIB_PREFIX, 'cache')
BIB_DEVKIT = os.path.join(BIB_PREFIX, 'devkit')

# Target architecture, read from either the environment or from the ARCH
# file created at install time.
try:
  BIB_ARCH = os.getenv('BIB_ARCH')
  if not BIB_ARCH:
    BIB_ARCH = open(os.path.join(BIB_PREFIX, 'ARCH')).readlines()[0].rstrip()
except:
  print "bib: error: couldn't read the target architecture from the " + \
        "BIB_ARCH environment variable or the ARCH file created at install time"
  sys.exit(1)

# Possible subdirectories to create symlinks into.
BIB_DIRS = \
  ['bin', 'lib', 'lib/pkgconfig', 'include', 'share'] + \
  [os.path.join('man', 'man%d' % i) for i in xrange(1,8)]

# vim: autoindent expandtab ts=2 shiftwidth=2
