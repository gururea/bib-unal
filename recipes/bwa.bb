[default]
name = bwa
homepage = http://bio-bwa.sourceforge.net/
toolchain = inplace
build_cmds = make CC=gcc
bin = bwa
man/man1 = bwa.1

[20140117_v_0.7.5a_v_ALL]
url = http://downloads.sourceforge.net/project/bio-bwa/bwa-0.7.5a.tar.bz2
sha1 = 3ba4a2df24dc2a2578fb399dc77b3c240a5a18be
