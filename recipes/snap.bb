[default]
name = snap
homepage = http://korflab.ucdavis.edu/software.html
toolchain = inplace
build_cmds = make
bin = exonpairs fathom forge hmm-info snap *.pl

[20131129_v_2013-11-29_v_ALL]
url = http://korflab.ucdavis.edu/Software/snap-2013-11-29.tar.gz
source_dir = snap
sha1 = 0ff0612ecb7040dfaa58b4330396d025abc0b758
