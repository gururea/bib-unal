[default]
name = macse
homepage = http://kimura.univ-montp2.fr/redmine/projects/macse
toolchain = inplace
bin = *
system-depends = =java/any

[20131201_v_0.9b1_v_ALL]
url = http://mbb.univ-montp2.fr/MBB/uploads/macse_v0.9b1.jar
sha1 = 36ba126fa7d77b22016b8f0bf72d5b13d1a6c4f4
postfix_cmds = echo -e "#!/bin/sh\njava -jar \$(which macse_v0.9b1.jar) \"\$@\"" >macse; chmod a+x macse macse_v0.9b1.jar
bin = macse macse_v0.9b1.jar
