[default]
name = seq-gen
homepage = http://tree.bio.ed.ac.uk/software/seqgen
toolchain = custom
build_cmds = cd source; make CLINKER=gcc; cp seq-gen %(prefix)s
bin = seq-gen

[20111028_v_1.3.3_v_ALL]
url = http://tree.bio.ed.ac.uk/download.php?id=85
filename = Seq-Gen.v1.3.3.tgz
sha1 = b96d353c963766d78bde47761bc6cecd6dae0372
