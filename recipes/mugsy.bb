[default]
name = mugsy
homepage = http://mugsy.sourceforge.net
toolchain = inplace
build_cmds = sed 's|/usr/local/projects/angiuoli/mugsy_trunk|%(prefix)s|' -i mugsy
bin = mugsy

[20121221_v_1.2.3_v_centos]
url = http://downloads.sourceforge.net/project/mugsy/mugsy_x86-64-v1r2.3.tgz
sha1 = fab41ab20c5e8753c14c89502f9d54752acb57c9

[20121221_v_1.2.3_v_ubuntu]
url = http://downloads.sourceforge.net/project/mugsy/mugsy_x86-64-v1r2.3.tgz
sha1 = fab41ab20c5e8753c14c89502f9d54752acb57c9
