[default]
name = jellyfish
homepage = http://www.cbcb.umd.edu/software/jellyfish/
toolchain = gnu
bin = bin/*
lib = lib/*.a lib/*.dylib lib/*.la
lib/pkgconfig = lib/pkgconfig/*
man/man1 = share/man/man1/*
include = include/jellyfish-1.1.11/jellyfish

[20140116_v_1.1.11_v_ALL]
url = http://www.cbcb.umd.edu/software/jellyfish/jellyfish-1.1.11.tar.gz
sha1 = 8bd6a7b382e94d37adfddd916dec2b0e36f840fd
