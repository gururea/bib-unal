[default]
name = biolite-tools
homepage = https://bitbucket.org/caseywdunn/biolite
toolchain = gnu
bin = bin/bl-*

[20130414_v_0.4.0_v_ALL]
url = https://bitbucket.org/caseywdunn/biolite/downloads/biolite-tools-0.4.0.tar.gz
sha1 = 644d01079a20302e6873a858fb570c31b60e7e8d
