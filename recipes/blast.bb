[default]
name = blast
homepage = http://blast.ncbi.nlm.nih.gov
toolchain = inplace
bin = bin/*

[20130106_v_2.2.29+_v_centos]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.29/ncbi-blast-2.2.29+-x64-linux.tar.gz
source_dir = ncbi-blast-2.2.29+

[20130106_v_2.2.29+_v_ubuntu]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.29/ncbi-blast-2.2.29+-x64-linux.tar.gz
source_dir = ncbi-blast-2.2.29+

[20130106_v_2.2.29+_v_osx]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.29/ncbi-blast-2.2.29+-universal-macosx.tar.gz
source_dir = ncbi-blast-2.2.29+
sha1=89b7722f9be47a882b23b50bc16b3a7bf9c68eaa
