[default]
name = sratoolkit
homepage = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/
toolchain = inplace
bin = bin/*[a-z]

[20131202_v_0.2.3.4-2_v_centos]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.3.4-2/sratoolkit.2.3.4-2-centos_linux64.tar.gz

[20131202_v_0.2.3.4-2_v_ubuntu]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.3.4-2/sratoolkit.2.3.4-2-ubuntu64.tar.gz

[20131202_v_0.2.3.4-2_v_osx]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.3.4-2/sratoolkit.2.3.4-2-mac64.tar.gz
sha1 = 35c9e93fd5f2c22dd92393562d52d7e345cdc20a
