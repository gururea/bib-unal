[default]
name = mummer
homepage = http://mummer.sourceforge.net
toolchain = inplace
build_cmds = make
bin = annotate combineMUMs delta-filter dnadiff exact-tandems gaps mapview mgaps mummer mummerplot nucmer nucmer2xfig promer repeat-match run-* show-*

[20111217_v_3.23_v_ALL]
url = https://downloads.sourceforge.net/project/mummer/mummer/3.23/MUMmer3.23.tar.gz
sha1 = 69261ed80bb77e7595100f0560973fe1f810c5fa
