[default]
name = samtools
homepage = http://sourceforge.net/projects/samtools/
toolchain = inplace
build_cmds = make; make razip; make -C bcftools
postfix_cmds = mkdir bam; mv *.h bam
bin = samtools razip bcftools/bcftools bcftools/vcfutils.pl misc/maq2sam-long misc/maq2sam-short misc/md5fa misc/md5sum-lite misc/wgsim misc/*.pl
lib = libbam.a
include = bam
man/man1 = samtools.1

[20130319_v_0.1.19_v_ALL]
url = http://downloads.sourceforge.net/project/samtools/samtools/0.1.19/samtools-0.1.19.tar.bz2
sha1 = ff3f4cf40612d4c2ad26e6fcbfa5f8af84cbe881
