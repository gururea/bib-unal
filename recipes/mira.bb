[default]
name = mira
homepage = http://sourceforge.net/projects/mira-assembler
toolchain = inplace
bin = bin/*

[20140418_v_4.0.2_v_centos]
url = http://downloads.sourceforge.net/project/mira-assembler/MIRA/stable/mira_4.0.2_linux-gnu_x86_64_static.tar.bz2
sha1 = 6e63f72171af7cb0ec2ce84db395cbb1abc44ad0

[20140418_v_4.0.2_v_ubuntu]
url = http://downloads.sourceforge.net/project/mira-assembler/MIRA/stable/mira_4.0.2_linux-gnu_x86_64_static.tar.bz2
sha1 = 6e63f72171af7cb0ec2ce84db395cbb1abc44ad0

[20140418_v_4.0.2_v_osx]
url = http://downloads.sourceforge.net/project/mira-assembler/MIRA/stable/mira_4.0.2_darwin13.1.0_x86_64_static.tar.bz2
