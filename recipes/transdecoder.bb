[default]
name = transdecoder
homepage = http://transdecoder.sourceforge.net/
toolchain = inplace
postfix_cmds = sed -e 's/FindBin::Bin/FindBin::RealBin/g' -i *.pl
bin = *.pl

[20120815_v_r2012-08-15_v_ALL]
url = http://downloads.sourceforge.net/project/transdecoder/OLDER/transdecoder_r2012-08-15.tgz
sha1 = 9b7dedb327761a2c328770fd53eaafd6b29b9ffa
