[default]
name = lastz
homepage = http://www.bx.psu.edu/~rsharris/lastz
toolchain = inplace
build_cmds = make definedForAll=-Wall installDir=%(prefix)s/bin install
bin = bin/*

[20100112_v_1.02.00_v_ALL]
url = http://www.bx.psu.edu/miller_lab/dist/lastz-1.02.00.tar.gz
source_dir = lastz-distrib-1.02.00
sha1 = 49680c6e18e2b1a417953107eedf41b2aece974d
