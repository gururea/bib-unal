[default]
name = muscle
homepage = http://www.drive5.com/muscle
toolchain = custom
build_cmds = cd src; sed -i~ -e 's/-static//' mk; make; cp muscle %(prefix)s
bin = muscle

[20140902_v_3.8.31_v_ALL]
url = http://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_src.tar.gz
sha1 = 2fe55db73ff4e7ac6d4ca692f8f213d1c5071dac
source_dir = muscle3.8.31
