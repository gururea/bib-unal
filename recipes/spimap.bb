[default]
name = spimap
homepage = http://compbio.mit.edu/spimap/
depends = *gsl/*
toolchain = custom
build_cmds = make CXX=g++; make install prefix=%(prefix)s
bin = bin/spimap*
lib = lib/libspidir*

[20110225_v_1.1_v_ALL]
url = http://compbio.mit.edu/spimap/pub/spimap-1.1.tar.gz
sha1 = 58f465a9e945bea8d4ed43114bbe174d9231c686
