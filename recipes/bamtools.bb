[default]
name = bamtools
homepage = https://github.com/pezmaster31/bamtools
toolchain = cmake
bin = bin/bamtools
lib = lib/bamtools/*
include = include/bamtools
test_inst_cmds = bamtools --version
system-depends = +cmake/any,+zlib/any,+g++/any

[20130724_v_2.3_v_ALL]
url = https://github.com/pezmaster31/bamtools/archive/v2.3.0.tar.gz
source_dir = bamtools-2.3.0
sha1 = 397d595b373ccc11503856d7e2804833aa8ea811
