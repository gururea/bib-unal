[default]
name = trinity
homepage = http://trinityrnaseq.sourceforge.net/
toolchain = inplace
build_cmds = make -B CXXFLAGS=-fopenmp

[20140423_v_r20140413p1_v_ALL]
url = http://downloads.sourceforge.net/trinityrnaseq/trinityrnaseq_r20140413p1.tar.gz
sha1 = d916cc7099a6a45de923e20bba85bb7949420395
postfix_cmds = sed -e 's/FindBin::Bin/FindBin::RealBin/g' -i~ Trinity
bin = Trinity

[20130814_v_r2013_08_14_v_ALL]
url = http://downloads.sourceforge.net/trinityrnaseq/trinityrnaseq_r2013_08_14.tgz
sha1 = 3ca275e79e7740974a949a7fbe469e2e74471e3f
bin = Trinity.pl Inchworm/bin/inchworm Chrysalis/Chrysalis Chrysalis/QuantifyGraph util/partition_chrysalis_graphs_n_reads.pl Butterfly/Butterfly.jar
