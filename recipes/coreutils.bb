[default]
name = coreutils
homepage = http://www.gnu.org/software/coreutils
toolchain = gnu
build_options = --disable-dependency-tracking --without-gmp
bin = bin/dir bin/dircolors bin/factor bin/hostid bin/md5sum bin/nproc bin/pinky bin/ptx bin/sha* bin/shred bin/shuf bin/tac bin/timeout bin/truncate bin/vdir

[20110908_v_8.13_v_osx]
url = http://ftpmirror.gnu.org/coreutils/coreutils-8.13.tar.gz
sha1 = 83b7e25661c439ecac55e99ff0dd816b9ff478a5
