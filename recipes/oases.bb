[default]
name = oases
homepage = http://www.ebi.ac.uk/~zerbino/oases/
toolchain = inplace
bin = oases

[20131201_v_0.2.08_v_ALL]
url = http://www.ebi.ac.uk/~zerbino/oases/oases_0.2.08.tgz
source_dir = oases_0.2.8
sha1 = a06a9888ff97e505af7eeb0f93bccfb0f00169b5
depends = *velvet/1.2.09
build_cmds = make OPENMP=1 MAXKMERLENGTH=127 LONGSEQUENCES=1 VELVET_DIR=%(BIB_INSTALL)s/velvet/1.2.09 oases
