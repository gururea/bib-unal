[default]
name = abyss
homepage = http://www.bcgsc.ca/platform/bioinfo/software/abyss
toolchain = gnu
depends = *google-sparsehash/*
build_options = --disable-dependency-tracking
bin = bin/*
man/man1 = share/man/man1/*
system-depends = +g++/any

[20131211_v_1.3.7_v_ALL]
url = http://www.bcgsc.ca/platform/bioinfo/software/abyss/releases/1.3.7/abyss-1.3.7.tar.gz
sha1 = 3d0831b1b7fd0601ded7c2d01938a24b3fc1931a
