[default]
name = bedtools
homepage = https://github.com/arq5x/bedtools2
toolchain = inplace
build_cmds = make
bin = bin/*

[20140523_v_2.20.1_v_ALL]
url = https://github.com/arq5x/bedtools2/releases/download/v2.20.1/bedtools-2.20.1.tar.gz
source_dir = bedtools2-2.20.1
sha1 = 4b7b5866199b0eefd093f39fef260dcd369ae13a
