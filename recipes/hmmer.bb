[default]
name = hmmer
homepage = http://hmmer.janelia.org/
toolchain = gnu
bin = bin/*

[20140117_v_3.0_v_ALL]
url = http://selab.janelia.org/software/hmmer3/3.0/hmmer-3.0.tar.gz
sha256 = 6977e6473fcb554b1d5a86dc9edffffa53918c1bd88d7fd20d7499f1ba719e83
