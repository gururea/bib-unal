[default]
name = raxml
homepage = https://github.com/stamatak/standard-RAxML
toolchain = custom
build_cmds = make -f Makefile.SSE3.PTHREADS.gcc CC=gcc CFLAGS=-pthread; make -f Makefile.SSE3.gcc CC=gcc CFLAGS=-pthread; cp raxmlHPC-PTHREADS-SSE3 raxmlHPC-SSE3 %(prefix)s
bin = raxmlHPC-PTHREADS-SSE3 raxmlHPC-SSE3

[20131201_v_7.7.6_v_ALL]
url = https://github.com/stamatak/standard-RAxML/archive/v7.7.6.zip
source_dir = standard-RAxML-7.7.6
sha1 = f83c56bacbd12204862aff4c4130a5e58c1305e6
