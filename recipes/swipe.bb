[default]
name = swipe
homepage = http://dna.uio.no/swipe/
toolchain = inplace
build_cmds = make swipe CXX=g++
bin = swipe

[20140210_v_2.0.9_v_ALL]
url = https://github.com/torognes/swipe/archive/v2.0.9.tar.gz
source_dir = swipe-2.0.9
sha256 = 4b0cab5337f759f52722e416071fd202be7929dd19f569dfd8df291133ea855e
