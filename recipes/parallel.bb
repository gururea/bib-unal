[default]
name = parallel
homepage = http://savannah.gnu.org/projects/parallel
toolchain = gnu
bin = bin/*
man/man1 = share/man/man1/*
cleanup_after_install = true

[20130922_v_20130922_v_ALL]
url = http://ftpmirror.gnu.org/parallel/parallel-20130922.tar.bz2
source_dir = parallel-20130922
sha256 = e8fa0e4dd06781aa90f4567493ae61233b8db6a1b35257f8d229f9efd737b909
