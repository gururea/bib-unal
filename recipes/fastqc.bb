[default]
name = fastqc
homepage = http://www.bioinformatics.babraham.ac.uk/projects/fastqc
toolchain = inplace
source_dir = FastQC
build_cmds = chmod a+x fastqc
bin = fastqc
system-depends = +java/any

[20131201_v_0.10.1_v_ALL]
url = http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.10.1.zip
sha1 = d1d9c1489c46458614fcedcbbb30e799a0e796a2
