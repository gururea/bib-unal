[default]
name = yasra
homepage = http://www.bx.psu.edu/miller_lab
depends = *lastz/*
toolchain = gnu
bin = bin/*

[20140327_v_2.33_v_ALL]
url = http://www.bx.psu.edu/miller_lab/dist/YASRA-2.33.tar.gz
sha1 = da25bcdbfa9971519a7a53077ad0930e2be50f2e
