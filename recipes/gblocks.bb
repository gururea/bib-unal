[default]
name = gblocks
homepage = http://molevol.cmima.csic.es/castresana/Gblocks.html
toolchain = inplace
bin = Gblocks

[20131201_v_0.91b_v_centos]
url = http://molevol.cmima.csic.es/castresana/Gblocks/Gblocks_Linux64_0.91b.tar.Z
source_dir = Gblocks_0.91b
sha1 = 149d8ae44346eab8e39b71309aa8961f37ffde76

[20131201_v_0.91b_v_ubuntu]
url = http://molevol.cmima.csic.es/castresana/Gblocks/Gblocks_Linux64_0.91b.tar.Z
source_dir = Gblocks_0.91b
sha1 = 149d8ae44346eab8e39b71309aa8961f37ffde76

[20131201_v_0.91b_v_osx]
url = http://molevol.cmima.csic.es/castresana/Gblocks/Gblocks_OSX_0.91b.tar.Z
source_dir = Gblocks_0.91b
sha1 = 2e453593516f44a71201eb6fcb8b1fbf2e868068
